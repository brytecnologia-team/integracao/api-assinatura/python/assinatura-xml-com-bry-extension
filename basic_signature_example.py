from flask import Flask, jsonify, request
from flask_cors import CORS
import requests
import base64


URL_INITIALIZATION = 'https://hub2.hom.bry.com.br/fw/v2/cms/pkcs1/assinaturas/acoes/inicializar'
URL_FINALIZATION = 'https://hub2.hom.bry.com.br/fw/v2/cms/pkcs1/assinaturas/acoes/finalizar'

app = Flask(__name__)
CORS(app, supports_credentials=True)

@app.route('/initialize', methods=['POST'])
def initialize_signature():
    
# Step 1 - Receiving the contents of the digital certificate and the document to be signed.
    data = request.form
    file = request.files['document'].read()

    original_documents = []
    original_documents.append(('originalDocuments[0][content]', file))

    form_initialization = {
        'nonce': 1, 
        'attached': 'false',
        'profile': 'BASIC',
        'binaryContent': 'false',
        'operationType': 'SIGNATURE',
        'originalDocuments[0][nonce]': '1',
        'signatureFormat': 'ENVELOPED',
        'hashAlgorithm': 'SHA256',
        'certificate': data['certificate'],
    }

# Step 2 - Initialization of the signature and production of the signedAttributes artifacts.

    response = requests.post(URL_INITIALIZATION, data = form_initialization, headers={ 'Authorization': request.headers['Authorization'] }, files=original_documents)

    if response.status_code == 200:
        reponse_data = response.json()

# Step 3 - Send the data for encryption at the frontend.

        return jsonify(reponse_data)
    
    else:
        return(response.text)

@app.route('/finalize', methods=['POST'])
def finalize_signature():
    print(request.headers)

# Step 4 - Receipt of encrypted data at the frontend.

    data = request.form
    file = request.files['document'].read()

    original_documents = []
    original_documents.append(('finalizations[0][document]', file))

    form_finalization = {
        'nonce': 1,
        'attached': 'false',
        'profile': 'BASIC', 
        'hashAlgorithm': 'SHA256',
        'binaryContent': 'false', 
        'signatureFormat': 'ENVELOPED', 
        'certificate': data['certificate'], 
        'operationType': 'SIGNATURE',
        'finalizations[0][nonce]': 1, 
        'finalizations[0][signedAttributes]': data['initializedDocuments'],
        'finalizations[0][signatureValue]': data['cifrado']
    }

# Step 5 - Finalization of the signature and obtaining the signed artifact.
    response = requests.post(URL_FINALIZATION, data = form_finalization, headers={ 'Authorization': request.headers['Authorization'] }, files=original_documents)
    print(response)
    print(response.text)
    print(response.content)
    if response.status_code == 200:
        data = response.json()
        base64_string = data['signatures'][0]['content']
        signature = base64.b64decode(base64_string.encode("utf-8"))
        signed_file = open('./files/signature.xml', 'wb')
        signed_file.write(signature)
        signed_file.close()
        return jsonify(data)

    else:
        print(response.text)

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=True)
    #app.run(debug=True)